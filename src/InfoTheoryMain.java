import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class InfoTheoryMain
{
  public InfoTheoryMain() {}
  
  public static void main(String[] args)
    throws IOException, InterruptedException
  {
    File logDir = new File("logs");
    if (!logDir.exists())
    {
      System.out.println("Log folder does not exist.");
      System.out.println("Creating it...");
      logDir.mkdirs();
    }
    
    Scanner scan = new Scanner(System.in);
    System.out.println("Please input the port information: (Default is /dev/cu.usbmodem1411)");
    String port = scan.nextLine();
    if ((port == null) || (port.equals("")))
    {
      System.out.println("empty port");
      port = "/dev/cu.usbmodem1411";
    }
    System.out.println("Input Participant number:");
    int participant = scan.nextInt();
    
    File logFile = new File("logs/P" + participant + ".csv");
    Experiment myExperiment; 
    if (logFile.exists())
    {
      int block = extractLastBlock(logFile);
      myExperiment = new Experiment(participant, logFile, block, port);
    }
    else
    {
      logFile.createNewFile();
      myExperiment = new Experiment(participant, logFile, port);
    }
    
    ExperimentWindow test = new ExperimentWindow(myExperiment);
    myExperiment.setWindow(test);
    myExperiment.startExperiment();
    scan.close();
  }
  
  public static int extractLastBlock(File name) throws FileNotFoundException, IOException
  {
    System.out.println("Logfile already exists. Finding out where to resume.");
    int block = -1;
    BufferedReader br = new BufferedReader(new FileReader(name));
    String line;
    while ((line = br.readLine()) != null) {
      if (!line.isEmpty())
      {
        String[] lineTokens = line.split(",");
        try {
          block = Integer.parseInt(lineTokens[1]);
        }
        catch (Exception localException) {}
      }
    }
    br.close();
    System.out.println("Resuming on block #" + block);
    return block;
  }
}