import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Experiment {


	public static final Random rand = new Random(System.currentTimeMillis());
	public static final int NUMBER_OF_BLOCKS = 5;
	public static final int REPETITIONS_PER_BLOCK = 1;
	public static final int NB_PATTERNS = 14;
	public static final int TARGET_DIAMETER = 270;
	public static final int TARGET_SIZE = 100;
	
	public static Image[] iconStimuli;
	
	public static final int[] PAUSE_DURATIONS = { 15, 30, 60, 100, 200};
	public static final int NUMBER_OF_DURATIONS = 5;
	
	public static final String[] PATTERN_NAMES = {
			"Short",
			"Long",
			"Short Short",
			"Short Long",
			"Long Short",
			"Long Long",
			"Short Short Short",
			"Short Short Long",
			"Short Long Short",
			"Short Long Long",
			"Long Short Short",
			"Long Short Long",
			"Long Long Short",
			"Long Long Long"
	};

	private boolean expeStarted = false;
	private boolean recordedReaction = false;
	private boolean recordedSelection = false;
	private boolean newBlock = false;
	private boolean expeEnded = false;
	private ExperimentWindow window;
	
	private ExpeState expe = ExpeState.STARTING;
	private File logFile;
	private PrintWriter pw;
	private ArduinoHandler arduino;
	private int participant;

	private int selectedIndex = -1;

	private int currentBlock = -1;
	private ArrayList<Block> listBlocks = new ArrayList<Block>();

	public Experiment(int _p, File log, String port) throws IOException
	{
		this.logFile = log;
		this.participant = _p;
		this.pw = new PrintWriter(new FileWriter(this.logFile, true));
		this.arduino = new ArduinoHandler(port);
		this.generateExperiment();
	}

	public Experiment(int _p, File log, int block, String port) throws IOException
	{
		this.logFile = log;
		this.participant = _p;
		this.pw = new PrintWriter(new FileWriter(this.logFile, true));
		this.currentBlock = block - 1;
		this.arduino = new ArduinoHandler(port);
		this.generateExperiment();
	}


	private void generateExperiment() throws IOException {
		iconStimuli = new Image[Experiment.NB_PATTERNS];
		// Adding pointing block
		this.listBlocks.add(new Block(false, true));
		// Adding training block
		this.listBlocks.add(new Block(true,false));
		for (int i=0; i < NUMBER_OF_BLOCKS-2; i++)
		{
			this.listBlocks.add(new Block());
		}
		
		for (int j = 0 ; j < Experiment.NB_PATTERNS; j++)
		{
			iconStimuli[j] = ImageIO.read(new File("img/" + j + ".png"));
		}
	}

	public void setWindow(ExperimentWindow frame)
	{
		this.window = frame;
	}

	public void startExperiment() throws InterruptedException
	{
		if (this.currentBlock == -1)
		{
			pw.println("Participant,Block,Trial,Training,Distraction,Sequence,StimulusIndex,StimulusFinger,Answer, Correct?,ReactionTime,SelectionTime,ExecutionTime");
			pw.flush();
		}
		while (!expeStarted)
		{
			Thread.sleep(100);
		}
		System.out.println("Starting the blocks! Total of " + this.listBlocks.size());
		this.nextBlock();
		for (int i=this.currentBlock; i < this.listBlocks.size(); i++, this.currentBlock++)
		{
			this.expe = ExpeState.INTERBLOCK;
			this.window.updateInstructions(this.expe);
			while (!newBlock)
			{
				Thread.sleep(100);
			}
			this.newBlock = false;
			System.out.println("Starting a new block!");
			Block b = this.getCurrentBlock();
			for (Trial t : this.getCurrentBlock().getTrialList())
			{
				this.expe = ExpeState.INTERTRIAL;
				this.window.updateInstructions(this.expe);
				Thread.sleep(1000);
				b.nextTrial();
				System.out.println("Starting the trials!");
				this.expe = ExpeState.STIMULUS_PLAYING;
				this.window.updateInstructions(this.expe);
				if (!this.getCurrentBlock().isPointingBlock())
				{
					this.playStimulus(t);
				}
				//Thread.sleep(600);
				this.expe = ExpeState.REACTION;
				this.window.updateInstructions(this.expe);
				b.getCurrentTrial().startTrial();
				/*while (!recordedReaction)
				{
					
				}*/
				this.recordedReaction = false;
				this.expe = ExpeState.SELECTION;
				this.window.updateInstructions(this.expe);
				b.getCurrentTrial().stopReactionTime();
				while (!recordedSelection)
				{
					Thread.sleep(100);
				}
				this.recordedSelection = false;
				this.expe = ExpeState.INTERTRIAL;
				b.getCurrentTrial().stopSelectionTime(this.selectedIndex);
				this.logTrial();
			}
			//this.nextBlock();
		}
		this.expe = ExpeState.FINISHED;
		this.window.updateInstructions(this.expe);
		this.pw.flush();
		this.pw.close();
		this.arduino.closePort();
		while (!expeEnded)
		{
			Thread.sleep(100);
		}
		System.exit(0);
	}

	private void playStimulus(Trial t) {
		this.arduino.sendString(t.getStimulusCode());
	}

	private void logTrial() {
		this.pw.println(this.getExperimentSummary());
		this.pw.flush();
		System.out.println(this.getExperimentSummary());

	}

	public void nextBlock()
	{
		this.currentBlock++;
	}

	public boolean isFinished()
	{
		return this.currentBlock == Experiment.NUMBER_OF_BLOCKS;
	}

	public String getExperimentSummary()
	{
		return "P" + this.participant + "," + this.currentBlock + "," + this.getCurrentBlock().getBlockSummary();
	}

	public Block getCurrentBlock() {
		return this.listBlocks.get(this.currentBlock);
	}

	public ExpeState getExpeState() {
		return this.expe;
	}

	public void UiStartExpe() {
		this.expeStarted = true;
	}

	public void UiReactionTime() {
		this.recordedReaction = true;
	}

	public void UiSelectionTime(int ans)
	{
		int tmp = ans;
		this.selectedIndex = tmp;
		this.recordedSelection = true;
	}

	public void UiNewBlock()
	{
		this.newBlock = true;
	}

	public void UiStopExpe()
	{
		this.expeEnded = true;
	}

	public String getBlockNumber() {
		// TODO Auto-generated method stub
		return this.currentBlock + "";
	}
	
	public String getTrialNumber()
	{
		return this.getCurrentBlock().getTrialNumber();
	}

	public void UiDisplayTargets() {
		// TODO Auto-generated method stub
		this.window.displayTargets();
	}

}
