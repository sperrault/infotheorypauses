import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class ExperimentWindow extends JFrame implements KeyListener, ActionListener, MouseListener {
	private static final long serialVersionUID = 1L;
	private Experiment exp;
	private JLabel instructions;
	private Point targetCoordinates[] = new Point[Experiment.NB_PATTERNS];
	private Point targetCenters[] = new Point[Experiment.NB_PATTERNS];
	private JPanel panelContainer = new JPanel();
	private TargetPanel targetContainer;
	private JPanel cards = new JPanel(new CardLayout());
	private JLabel pointingInstructions = new JLabel("Pointing Instructions");
	
	public static final String INSTRUCTIONS_TEXT = "INSTRUCTIONS";
	public static final String TARGETS_TEXT = "TARGETS";
	
	public ExperimentWindow(Experiment _e){
		this.exp = _e;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.instructions = new JLabel("Press Space to start the experiment", SwingConstants.CENTER);
		this.instructions.setFont(new Font(this.instructions.getFont().toString(), Font.PLAIN, 32));
		this.panelContainer.add(this.instructions);

		this.targetContainer = new TargetPanel(this.targetCoordinates);
		this.targetContainer.setLayout(new BorderLayout());
		this.targetContainer.add(this.pointingInstructions, BorderLayout.NORTH);
		this.targetContainer.setSize(new Dimension(900,800));
		this.targetContainer.setPreferredSize(new Dimension(900,800));
		this.targetContainer.addMouseListener(this);
		int xCenter = 450;
		int yCenter = 350;
		for (int i=0; i < Experiment.NB_PATTERNS; i++)
		{
			double angle = (Math.PI * 2) / Experiment.NB_PATTERNS;
			int xOffset = (int) (xCenter + (Experiment.TARGET_DIAMETER * Math.cos(i * angle)));
			int yOffset = (int) (yCenter - (Experiment.TARGET_DIAMETER * Math.sin(i * angle)));
			this.targetCenters[i] = new Point(xOffset, yOffset);
			xOffset -= (Experiment.TARGET_SIZE / 2);
			yOffset -= (Experiment.TARGET_SIZE / 2);
			this.targetCoordinates[i] = new Point(xOffset, yOffset);
			//System.out.println(i + " = (" + xOffset + ", " + yOffset + ")");
//			this.targets[i].setBounds(xOffset,yOffset, Experiment.TARGET_SIZE, Experiment.TARGET_SIZE);
		}

//		this.targetContainer.setLayout(null);

		this.cards.add(this.panelContainer, INSTRUCTIONS_TEXT);
		this.cards.add(this.targetContainer, TARGETS_TEXT);
		this.getContentPane().add(this.cards, null);
		//this.getContentPane().add(instructions, BorderLayout.CENTER);
		this.setSize(new Dimension(900,800));
		this.setPreferredSize(new Dimension(900,800));

		//int xCenter = this.targetContainer.getWidth() / 2;
		//int yCenter = this.targetContainer.getHeight() / 2;
		this.pointingInstructions.setFont(new Font(this.pointingInstructions.getFont().toString(), Font.PLAIN, 32));
		//this.pointingInstructions.setBounds(2, 2, 890, 32);
		this.pack();
		this.addKeyListener(this);
		//this.displayTargets();
		this.setVisible(true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_SPACE)
		{
			//System.out.println("Space! " + this.exp.getExpeState());
			// Case 1: Experiment needs to start
			if (this.exp.getExpeState() == ExpeState.STARTING)
			{
				this.exp.UiStartExpe();
			}
			else if ((this.exp.getExpeState() == ExpeState.REACTION) || (this.exp.getExpeState() == ExpeState.STIMULUS_PLAYING))
			{
				this.exp.UiReactionTime();
			}
			else if (this.exp.getExpeState() == ExpeState.INTERBLOCK)
			{
				this.exp.UiNewBlock();
			}
			else if (this.exp.getExpeState() == ExpeState.FINISHED)
			{
				this.exp.UiStopExpe();
			}

		}
		/*
		else if (this.exp.getExpeState() == ExpeState.SELECTION)
		{
			if (key == KeyEvent.VK_F)
			{
				this.exp.UiSelectionTime(0);
			}
			else if (key == KeyEvent.VK_G)
			{
				this.exp.UiSelectionTime(1);
			}
			else if (key == KeyEvent.VK_H)
			{
				this.exp.UiSelectionTime(2);
			}
			else if (key == KeyEvent.VK_J)
			{
				this.exp.UiSelectionTime(3);
			}
		
		}*/
		
	}
	
	public void updateInstructions(ExpeState exp)
	{
		this.centerMouse();
		if (exp == ExpeState.STARTING)
		{
			this.instructions.setText("Press the SPACE key");
		}
		else if (exp == ExpeState.INTERBLOCK)
		{
			this.instructions.setText("Press SPACE to start the next block");
		}
		else if ((exp == ExpeState.STIMULUS_PLAYING) || (exp == ExpeState.REACTION))
		{
			//this.instructions.setText("Block #" + this.exp.getBlockNumber() + ", Trial #" + this.exp.getTrialNumber());
			this.exp.UiDisplayTargets();

		}
		else if (exp == ExpeState.SELECTION) 
		{
			this.instructions.setText(" ");
			//this.pointingInstructions.setText(" ");
		}
		else if (exp == ExpeState.INTERTRIAL)
		{
			this.instructions.setText(" ");
		}
		else if (exp == ExpeState.FINISHED)
		{
			this.instructions.setText("Experiment Finished. Press SPACE");
		}
		this.instructions.repaint();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void centerMouse() {
		try {
		    Robot robot = new Robot();
		    robot.mouseMove(this.getX() + 450, this.getY() + 370);
		} catch (AWTException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	}
	
	public void displayTargets() {
		CardLayout card = (CardLayout) this.cards.getLayout();
		//this.pointingInstructions.setText("");
		card.show(cards, TARGETS_TEXT);
		if (this.exp.getCurrentBlock().isPointingBlock())
		{
			int stim = this.exp.getCurrentBlock().getCurrentTrial().getStimulus();
			this.displayPointingInstructions(stim);
			this.pointingInstructions.repaint();
			//this.targets[stim].setIcon(Experiment.targetStimuli[stim]);
		}
		else
		{
			this.pointingInstructions.setText("");
		}
		
	}

	public void displayInstructions() {
		CardLayout card = (CardLayout) this.cards.getLayout();
		card.show(cards, INSTRUCTIONS_TEXT);
		this.requestFocus();
	}
	
	public void displayPointingInstructions(int i) {
		Dimension d = this.getSize();
		this.pointingInstructions.setText("Please select " + Experiment.PATTERN_NAMES[i]);
		this.setSize(new Dimension((int)d.getWidth(),(int)d.getHeight()-1));
		this.setSize(d);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		int x = e.getX();
		int y = e.getY();
		//System.out.println("Clicked on coordinates " + e.getX() + "," + e.getY());
		for (int i=0; i < Experiment.NB_PATTERNS; i++)
		{
			if (Point.isWithinCircle(this.targetCenters[i], x, y))
			{
				//System.out.println("Clicked on target " + i + " (" + x + "," + y + ")");
				this.exp.UiSelectionTime(i);
				this.displayInstructions();
				return;
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
