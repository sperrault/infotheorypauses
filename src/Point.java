
public class Point {

	private int x;
	private int y;
	
	public Point (int _x, int _y)
	{
		this.x = _x;
		this.y = _y;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public static int distanceFrom(Point p, int x, int y)
	{
		return (int) Math.sqrt(Math.pow(p.getX() - x,2) + Math.pow(p.getY() - y,2));
	}
	
	public static boolean isWithinCircle(Point p, int x, int y)
	{
		return distanceFrom(p, x, y) <= (Experiment.TARGET_SIZE / 2);
	}
	
}
