import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class TargetPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Point[] targetCoordinates;
	private Image[] img;
	
	public TargetPanel(Point[] _x)
	{
		this.targetCoordinates = _x;
	}
	
	
	@Override
	public void paintComponent(Graphics g){
		for (int i=0; i < Experiment.NB_PATTERNS; i++)
		{
			int x = this.targetCoordinates[i].getX();
			int y = this.targetCoordinates[i].getY();
			g.drawImage(Experiment.iconStimuli[i], x, y, null);
		}
	}
}
