import jssc.SerialPort;
import jssc.SerialPortException;

/**
 *
 * @author Simon
 */
public class ArduinoHandler {
    
    private String portName;
    private SerialPort serialPort;
    
    public ArduinoHandler(String _c)
    {
        this.portName = _c;
        this.serialPort = new SerialPort(portName);
        try {
            System.out.println("Port opened: " + serialPort.openPort());
            System.out.println("Params setted: " + serialPort.setParams(SerialPort.BAUDRATE_115200,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE));
            Thread.sleep(1500);
        }
        catch (SerialPortException ex){
            ex.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public SerialPort getSerialPort()
    {
        return this.serialPort;
    }
    
    public void closePort() {
        try {
            this.serialPort.closePort();
        } catch (SerialPortException ex) {
            ex.printStackTrace();
        }
    }
    
    // delay-pin-frequency-time-intensity-(l/-)
    
    public void sendString(String msg)
    {
        try {
            this.serialPort.writeString(msg);
        } catch (SerialPortException ex) {
            ex.printStackTrace();
        }
    }
    
    
}
