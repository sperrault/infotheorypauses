import java.util.ArrayList;
import java.util.Collections;

public class Block {

	private boolean isTraining = false;
	private boolean isPointingBlock = false;
	private int currentIndex = -1;
	private ArrayList<Trial> listTrials = new ArrayList<Trial>();
	
	public Block()
	{
		this.generateBlock();
	}
	
	public Block(boolean _it, boolean _ip)
	{
		this.isTraining = _it;
		this.isPointingBlock = _ip;
		this.generateBlock();
	}
	
	public void generateBlock()
	{
		for (int i=0; i < Experiment.REPETITIONS_PER_BLOCK; i++)
		{
			for (int j=0; j < Experiment.NB_PATTERNS; j++)
			{
				for (int k=0 ; k < Experiment.NUMBER_OF_DURATIONS; k++)
				{
					this.listTrials.add(new Trial(j, Experiment.PAUSE_DURATIONS[k]));
				}
			}
		}
	
		Collections.shuffle(this.listTrials);
	}
	
	public Trial getCurrentTrial()
	{
		if (currentIndex > -1)
		{
			return this.listTrials.get(currentIndex);
		}
		return null;
	}
	
	public void nextTrial()
	{
		this.currentIndex++;
	}
	
	public boolean isFinished()
	{
		int totalTrials = Experiment.NB_PATTERNS * Experiment.NUMBER_OF_DURATIONS * Experiment.REPETITIONS_PER_BLOCK;
		return this.currentIndex == totalTrials;
	}
	
	public String getBlockSummary()
	{
		return this.currentIndex + "," + this.isTraining +  "," + this.isPointingBlock + "," + this.getCurrentTrial().getTrialSummary();
	}

	public ArrayList<Trial> getTrialList() {
		return this.listTrials;
	}

	public String getTrialNumber() {
		return this.currentIndex + "";
	}
	
	public boolean isPointingBlock() {
		return this.isPointingBlock;
	}
}
