import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Trial {

	private boolean isDistraction = false;
	private int stimulus = -1;
	private int answered = -1;
	private long startTrial;
	private long reactionTime;
	private long selectionTime;
	private boolean success;
	private boolean isDone = false;
	private int pause = 0;
	
	public Trial(int stim, int pause)
	{
		this.stimulus = stim;
		this.pause = pause;
	}
	
	public Trial(int stim, int pause, boolean _d)
	{
		this(stim, pause);
		this.isDistraction = _d;
		//this.generateTrial();
	}

	
	public String getStimulusCode()
	{
		String ans = "";
		for (int i=0; i < Experiment.PAUSE_DURATIONS.length; i++)
		{
			if (this.pause == Experiment.PAUSE_DURATIONS[i])
			{
				ans += (i + "");
			}
		}
		char stim = (char) (this.stimulus + 'a');
		ans += stim; 
		return ans;
	}
	
	public void startTrial()
	{
		this.startTrial = System.currentTimeMillis();
	}
	
	public void stopReactionTime()
	{
		this.reactionTime = System.currentTimeMillis();
	}
	
	public void stopSelectionTime(int value)
	{
		this.answered = value;
		this.success = (this.answered == this.stimulus);
		this.selectionTime = System.currentTimeMillis();
		this.isDone = true;
	}
	
	public boolean IsDone()
	{
		return this.isDone;
	}
	
	public String getTrialSummary()
	{
		double reaction = (this.selectionTime - this.startTrial) / 1000.0;
		String ans = this.answered + "";
		// distraction, sequence, stimulus index, finger stimulus, answer, success, reaction time, selection time, total time
		return this.isDistraction + "," + this.stimulus + "," + this.pause + "," + this.getStimulusCode()  +  "," + ans + "," + this.success + "," + reaction; 
	}
	
	
	public boolean getDistraction()
	{
		return this.isDistraction;
	}

	public int getStimulus() {
		return this.stimulus;
	}

}
